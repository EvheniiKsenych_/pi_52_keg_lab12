﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_12_console
{
    class Train
    {
        public Train()
        {
            Dest = "N/A";
            number = 0;
            time = 0;
            count_All = 0;
            count_Cupe = 0;
            count_Plac = 0;
        }
        public Train(string Destenation, int number_, int time_, int count_All_, int count_Cupe_, int count_Plac_)
        {
            Dest = Destenation;
            number = number_;
            time = time_;
            count_All = count_All_;
            count_Cupe = count_Cupe_;
            count_Plac = count_Plac_;
        }
        public Train(Train tmp)
        {
            Dest = tmp.Dest;
            number = tmp.number;
            time = tmp.time;
            count_All = tmp.count_All;
            count_Cupe = tmp.count_Cupe;
            count_Plac = tmp.count_Plac;
        }
        private string Dest;
        private int number;
        private int time;
        private int count_All;
        private int count_Cupe;
        private int count_Plac;

        public void Show()
        {
            string a = "Destenation: " + Dest + Environment.NewLine;
            a = "Destenation: " + Dest + Environment.NewLine;
            a += "Flight number: " + number.ToString() + Environment.NewLine;
            a += "Road time: " + time.ToString() + Environment.NewLine;
            a += "Count place: " + count_All.ToString() + Environment.NewLine;
            a += "Count cupe place: " + count_Cupe.ToString() + Environment.NewLine;
            a += "Count plac place: " + count_Plac.ToString() + Environment.NewLine;
            Console.WriteLine(a);
        }

        public void Default()
        {
            Dest = "N/A";
            number = 0;
            time = 0;
            count_All = 0;
            count_Cupe = 0;
            count_Plac = 0;
        }

        public string Destenation
        {
            get { return Dest; }
            set { Dest = value; }
        }
        public int Reis_Number
        {
            get { return number; }
            set { number = value; }
        }
        public int Road_Time
        {
            get { return time; }
            set { time = value; }
        }
        public int Plase_Count
        {
            get { return count_All; }
            set { count_All = value; }
        }
        public int Plase_Count_Cupe
        {
            get { return count_Cupe; }
            set { count_Cupe = value; }
        }
        public int Plase_Count_Plac
        {
            get { return count_Plac; }
            set { count_Plac = value; }
        }
    }
}
