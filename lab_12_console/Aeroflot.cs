﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_12_console
{
    class Aeroflot
    {
        public Aeroflot() {
            Dest = "N/A";
            number = 0;
            Tipe = "N/A";
            time = 0;
            Day = new int[0];
        }
        public Aeroflot(string Destenation, int number_, string Tipe_, int time_, int[]Day_)
        {
            Dest = Destenation;
            number = number_;
            Tipe = Tipe_;
            time = time_;
            Day = Day_;
        }
        public Aeroflot(Aeroflot tmp)
        {
            Dest = tmp.Dest;
            number = tmp.number;
            Tipe = tmp.Tipe;
            time = tmp.time;
            Day = tmp.Day;
        }
        public void Defoault()
        {
            Dest = "N/A";
            number = 0;
            Tipe = "N/A";
            time = 0;
            Day = new int[0];
        }
        private string Dest;
        private int number;
        private string Tipe;
        private int time;
        private int[] Day;

        public void Show()
        {
            string a =  "Destenation: " + Dest + Environment.NewLine;
            a += "Flight number: " + number.ToString() + Environment.NewLine;
            a += "Type of plane: " + Tipe + Environment.NewLine;
            a += "Time in air: " + time.ToString() + Environment.NewLine;
            a += "Day in fly: ";
            for (int i = 0; i < Day.Length; i++) a += Day[i]+" ";
            Console.WriteLine(a);
        }

        public string Destenation
        {
            get { return Dest; }
            set { Dest = value; }
        }
        public int Reis_Number
        {
            get { return number; }
            set { number = value; }
        }
        public string Plane_Tipe
        {
            get { return Tipe; }
            set { Tipe = value; }
        }
        public int Fly_Time
        {
            get { return time; }
            set { time = value; }
        }
        public int[] Fly_Day
        {
            get { return Day; }
            set { Day = value; }
        }
    }
}
