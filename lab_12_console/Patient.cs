﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_12_console
{
    class Patient
    {
        public Patient()
        {
            Surname = "N/A";
            Name = "N/A";
            Patronymic = "N/A";
            Adres = "N/A";
            Cart_Number = 0;
            Diagnosis = "N/A";
        }
        public Patient(string Surname_, string Name_, string Patronymic_, string Adres_, int Cart_Number_, string Diagnosis_)
        {
            Surname = Surname_;
            Name = Name_;
            Patronymic = Patronymic_;
            Adres = Adres_;
            Cart_Number = Cart_Number_;
            Diagnosis = Diagnosis_;
        }
        public Patient(Patient tmp)
        {
            Surname = tmp.Surname;
            Name = tmp.Name;
            Patronymic = tmp.Patronymic;
            Adres = tmp.Adres;
            Cart_Number = tmp.Cart_Number;
            Diagnosis = tmp.Diagnosis;
        }
        private string Surname;
        private string Name;
        private string Patronymic;
        private string Adres;
        private int Cart_Number;
        private string Diagnosis;

        public void Show()
        {
            string a = "Surname: " + Surname + Environment.NewLine;
            a += "Name: " + Name + Environment.NewLine;
            a += "Patronymic: " + Patronymic + Environment.NewLine;
            a += "Adres: " + Adres + Environment.NewLine;
            a += "Cart_Number: " + Cart_Number.ToString() + Environment.NewLine;
            a += "Diagnosis: " + Diagnosis + Environment.NewLine;
            Console.WriteLine(a);
        }
        public void Default()
        {
            Surname = "N/A";
            Name = "N/A";
            Patronymic = "N/A";
            Adres = "N/A";
            Cart_Number = 0;
            Diagnosis = "N/A";
        }
        public string surname
        {
            get { return Surname; }
            set { Surname = value; }
        }
        public string name
        {
            get { return Name; }
            set { Name = value; }
        }
        public string patronymic
        {
            get { return Patronymic; }
            set { Patronymic = value; }
        }
        public string adres
        {
            get { return Adres; }
            set { Adres = value; }
        }
        public int cart_number
        {
            get { return Cart_Number; }
            set { Cart_Number = value; }
        }
        public string diagnosis
        {
            get { return Diagnosis; }
            set { Diagnosis = value; }
        }
    }
}
